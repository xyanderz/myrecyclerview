package com.yan.myrecyclerview

data class Hero(
    var name: String = "",
    var detail: String = "",
    var photo: Int = 0
)